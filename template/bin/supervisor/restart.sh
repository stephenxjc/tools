echo before stop
sh status.sh

echo stop...
sh stop.sh

echo after stop
sh status.sh

echo before start
sh status.sh

echo start...
sh start.sh

echo after start
sh status.sh
