# 1. Tools

![image](https://www.travis-ci.org/xiewendan/tools.svg?branch=master)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Normal tools for daily work

## 1.1. Table of Contents

<!-- TOC -->

- [1. Tools](#1-tools)
    - [1.1. Table of Contents](#11-table-of-contents)
    - [1.2. Install](#12-install)
    - [1.3. Usage](#13-usage)
    - [1.4. Related Efforts](#14-related-efforts)
    - [1.5. Maintainers](#15-maintainers)
    - [1.6. Contributing](#16-contributing)
    - [1.7. License](#17-license)

<!-- /TOC -->


## 1.2. Install

* install [python3](https://www.python.org/downloads/)

* pip install: enter one folder in tool
  ```
  pip3 install -r requirements.txt
  ```

## 1.3. Usage

* create new tool
  ```
  python tool/new_tool/new_tool.py
  ```
  use the framework to implement your logic
  

* use exist tool
  * enter one folder in tool
  * read the readme.md

## 1.4. Related Efforts

## 1.5. Maintainers

[@xiewendan](https://github.com/xiewendan).

## 1.6. Contributing

PRs accepted.

## 1.7. License

[MIT © xiewendan](LICENSE)
