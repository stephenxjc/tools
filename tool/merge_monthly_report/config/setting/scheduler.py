# -*- coding: utf-8 -*-

Key0 = 'id'
Key1 = 'date'
Key2 = 'calendar_type'
Key3 = 'time'
Key4 = 'weekday'
Key5 = 'pre_notify'
Key6 = 'cycle_type'
Key7 = 'msg'

scheduler = {

	1: {
		Key0: 1,
		Key1: '2020-3-1',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'yearly',
		Key7: '我今天生日',
	},

	2: {
		Key0: 2,
		Key1: '2020-3-8',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 259200,
		Key6: 'yearly',
		Key7: '蛋仔3天后生日',
	},

	3: {
		Key0: 3,
		Key1: '2020-5-22',
		Key2: 'solar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '蛋仔明天后生日',
	},

	4: {
		Key0: 4,
		Key1: '2020-7-2',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '老妈明天生日',
	},

	5: {
		Key0: 5,
		Key1: '2020-9-10',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '老姐明天生日',
	},

	6: {
		Key0: 6,
		Key1: '2020-10-7',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '老姐、外婆明天生日',
	},

	7: {
		Key0: 7,
		Key1: '2020-5-20',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '奶奶明天生日',
	},

	8: {
		Key0: 8,
		Key1: '2020-10-8',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '小悦子明天生日',
	},

	9: {
		Key0: 9,
		Key1: '2020-10-10',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '小皮球明天生日',
	},

	10: {
		Key0: 10,
		Key1: '2020-6-28',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '桐桐明天生日',
	},

	11: {
		Key0: 11,
		Key1: '2020-12-3',
		Key2: 'lunar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 86400,
		Key6: 'yearly',
		Key7: '小橙子明天生日',
	},

	12: {
		Key0: 12,
		Key1: '2020-12-10',
		Key2: 'solar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'monthly',
		Key7: '存钱到工行两张卡，买流量，房租，还信用卡，还花呗',
	},

	13: {
		Key0: 13,
		Key1: '2021-3-30',
		Key2: 'solar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'once',
		Key7: '取消移动89元套餐',
	},

	14: {
		Key0: 14,
		Key1: '2020-4-23',
		Key2: 'solar',
		Key3: '12:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'monthly',
		Key7: '公司打卡提醒',
	},

	15: {
		Key0: 15,
		Key1: '2020-5-1',
		Key2: 'solar',
		Key3: '10:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'monthly',
		Key7: '团队月初报告提醒',
	},

	16: {
		Key0: 16,
		Key1: '2020-1-1',
		Key2: 'solar',
		Key3: '10:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'monthly',
		Key7: 'PMP PDU找曾老师更新',
	},

	17: {
		Key0: 17,
		Key1: '2020-12-1',
		Key2: 'solar',
		Key3: '10:0:0',
		Key4: 1,
		Key5: 0,
		Key6: 'once',
		Key7: '领取社保卡',
	},

}
